package com.ludmila.homework13;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Main {
    public static void main (String[] args) {
        /*TODO Lusinda_K:add -ea to VM option to activate assert key-word*/
        int[] array = IntStream.iterate(0, index -> index + 1)
                .limit(10)
                .toArray();
        int[] honestArray = Sequence.filter(array, element -> element % 2 == 0 && element != 0);
        Arrays.stream(honestArray)
                .forEach(element -> {
                    assert (element % 2 == 0);
                });
        Arrays.stream(honestArray).forEach(System.out::print);
        ByCondition predicate = number -> {
            int x = number, z = 0;
            while (x > 0) {
                z += x % 10;
                x /= 10;
            }
            return z % 2 == 0;
        };
        int[] filterArray = Sequence.filter(array, predicate);
        assert (filterArray.length == 5);
    }
}
