package homework16;

import java.util.stream.LongStream;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        final byte MASSIVE_SIZE = 20;
        ArrayList<Long> arrayList = new ArrayList<>();
        LongStream.iterate(0, index -> index + 1)
                .limit(MASSIVE_SIZE)
                .forEach(arrayList::add);
        /*удалим 11-й элемент*/
        System.out.println(arrayList);
        arrayList.removeAt(11);
        System.out.println(arrayList);
        try {
            arrayList.removeAt(999);
        } catch (ArrayIndexOutOfBoundsException exception) {
            System.out.println(exception.getMessage());
        }

        LinkedList<Long> linkedList = new LinkedList<>();
        LongStream.iterate(0, index -> index + 1)
                .limit(MASSIVE_SIZE)
                .forEach(linkedList::add);
        Long element = linkedList.get(11);
        System.out.println(element);
        try {
            linkedList.get(-150);
        } catch (ArrayIndexOutOfBoundsException exception) {
            System.out.println(exception.getMessage());
        }
    }
}

